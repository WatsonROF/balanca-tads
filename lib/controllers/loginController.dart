import 'package:get/get.dart';

class LoginController extends GetxController {
  final _isVisible = false.obs;

  get isVisible => this._isVisible.value;
  set isVisible(value) => this._isVisible.value = value;

  visivel() {
    isVisible = !isVisible;
    print(isVisible);
  }
}
