import 'dart:convert';

import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:http/http.dart' as http;

class ListaPesagens extends StatefulWidget {
  // const ListaPesagens({Key? key}) : super(key: key);

  @override
  _ListaPesagensState createState() => _ListaPesagensState();
}

class _ListaPesagensState extends State<ListaPesagens> {
  dynamic pesagem = 0;
  dynamic pesagens;
  bool _giveReverse = false;

  @override
  void initState() {
    // TODO: implement initState
    Pesagens();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: pesagens == null
          ? Container(
              alignment: Alignment.center,
              color: Colors.white,
              child: CircularProgressIndicator(),
            )
          : Container(
              width: MediaQuery.of(context).size.width * 1,
              height: MediaQuery.of(context).size.height * 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Column(
                      children: [
                        Container(
                          child: Image.asset(
                            'assets/image/84dd58f2-9e28-4db1-9582-e43d4e685074-profile-picture.jpeg',
                            height: 150,
                          ),
                        ),
                        // Container(
                        //   alignment: Alignment.center,
                        //   width: MediaQuery.of(context).size.width * 1,
                        //   height: 45,
                        //   color: Colors.green,
                        //   child: Text(
                        //     'Conectado',
                        //     style: TextStyle(
                        //       fontSize: 23,
                        //       fontWeight: FontWeight.bold,
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                  Text(
                    "Lista de Pesagens",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 3),
                    ),
                    height: MediaQuery.of(context).size.height * 0.55,
                    child: ListView.builder(
                      padding: EdgeInsets.all(0),
                      shrinkWrap: true,
                      itemCount: pesagens.length == 0
                          ? 1
                          : pesagens["pesagens"].length,
                      itemBuilder: (_, index) {
                        return Card(
                          elevation: 10,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      child: Text(
                                        'Brinco: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        '${pesagens["pesagens"][index]["tag"]}'
                                            .substring(10, 16),
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                      child: Text(
                                        'Data: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        '${pesagens["pesagens"][index]["date"]}',
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                      child: Text(
                                        'Peso: ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        '${pesagens["pesagens"][index]["peso"]}',
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        // Container(
                        //   child: Text(
                        //     'Aguardando passagem...',
                        //     style:
                        //         TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                        //   ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          height: 45,
                          width: MediaQuery.of(context).size.width * 1,
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              'Voltar',
                              style: TextStyle(fontSize: 22),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
    );
  }

  Future<http.Response> Pesagens() async {
    var response = await http.get(
      Uri.parse('http://192.168.4.1/pb/pesagens'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      },
    );
    if (response.statusCode >= 200 && response.statusCode <= 300) {
      pesagens = json.decode(response.body);
      setState(() {
        pesagens = pesagens;
      });
      print(pesagens["pesagens"]);
    }
    return response;
  }
}
