import 'package:flutter/material.dart';
import 'package:app_grupo_3/views/calibracao.dart';
import 'package:app_grupo_3/views/configuracaoWifi.dart';
import 'package:app_grupo_3/views/referencias.dart';

class Configuracao extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: MediaQuery.of(context).size.width * 1,
        height: MediaQuery.of(context).size.height * 1,
        child: Column(
          children: [
            Container(
              child: Image.asset(
                'assets/image/84dd58f2-9e28-4db1-9582-e43d4e685074-profile-picture.jpeg',
                height: 200,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              height: 45,
              width: MediaQuery.of(context).size.width * 1,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => ConfiguracaoWifi()));
                },
                child: Text(
                  'Wifi',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              height: 45,
              width: MediaQuery.of(context).size.width * 1,
              child: RaisedButton(
                onPressed: null,
                child: Text(
                  'Bluetooth',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              height: 45,
              width: MediaQuery.of(context).size.width * 1,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Calibracao()));
                },
                child: Text(
                  'Calibração',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              height: 45,
              width: MediaQuery.of(context).size.width * 1,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Referencias()));
                },
                child: Text(
                  'Referências',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  height: 45,
                  width: MediaQuery.of(context).size.width * 1,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Voltar',
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
