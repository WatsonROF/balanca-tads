import 'dart:convert';

import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:http/http.dart' as http;

class Pesagem extends StatefulWidget {
  // const Pesagem({Key? key}) : super(key: key);

  @override
  _PesagemState createState() => _PesagemState();
}

class _PesagemState extends State<Pesagem> {
  dynamic pesagem = 0;

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   carregaPesagens();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: MediaQuery.of(context).size.width * 1,
        height: MediaQuery.of(context).size.height * 1,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Column(
                children: [
                  Container(
                    child: Image.asset(
                      'assets/image/84dd58f2-9e28-4db1-9582-e43d4e685074-profile-picture.jpeg',
                      height: 150,
                    ),
                  ),
                  // Container(
                  //   alignment: Alignment.center,
                  //   width: MediaQuery.of(context).size.width * 1,
                  //   height: 45,
                  //   color: Colors.green,
                  //   child: Text(
                  //     'Conectado',
                  //     style: TextStyle(
                  //       fontSize: 23,
                  //       fontWeight: FontWeight.bold,
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
            Text(
              "Peso: $pesagem KG",
              style: TextStyle(
                fontSize: 26,
                fontWeight: FontWeight.bold,
              ),
            ),
            Container(
              child: Column(
                children: [
                  // Container(
                  //   child: Text(
                  //     'Aguardando passagem...',
                  //     style:
                  //         TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  //   ),
                  // ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    height: 45,
                    width: MediaQuery.of(context).size.width * 1,
                    child: RaisedButton(
                      onPressed: peso,
                      child: Text(
                        'Leitura',
                        style: TextStyle(fontSize: 22),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    height: 45,
                    width: MediaQuery.of(context).size.width * 1,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Voltar',
                        style: TextStyle(fontSize: 22),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<http.Response> peso() async {
    var response = await http.get(
      Uri.parse('http://192.168.4.1/pb/peso'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      },
    );
    if (response.statusCode >= 200 && response.statusCode <= 300) {
      pesagem = json.decode(response.body);
      setState(() {
        pesagem = pesagem["pesagem"];
      });
      print(pesagem);
    }
    return response;
  }
}
