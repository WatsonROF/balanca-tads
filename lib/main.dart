import 'package:flutter/material.dart';
import 'package:app_grupo_3/views/calibracao.dart';
import 'package:app_grupo_3/views/login.dart';
import 'package:app_grupo_3/views/referencias.dart';

main() {
  runApp(
    MaterialApp(
      home: Login(),
      debugShowCheckedModeBanner: false,
    ),
  );
}
