import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:app_grupo_3/views/configuracao.dart';
import 'package:app_grupo_3/views/listaPesagens.dart';
import 'package:app_grupo_3/views/pesagem.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: MediaQuery.of(context).size.width * 1,
        height: MediaQuery.of(context).size.height * 1,
        child: Column(
          children: [
            Container(
              child: Image.asset(
                'assets/image/84dd58f2-9e28-4db1-9582-e43d4e685074-profile-picture.jpeg',
                height: 200,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              width: MediaQuery.of(context).size.width * 1,
              height: 45,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Pesagem()));
                },
                child: Text(
                  'Pesar',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              width: MediaQuery.of(context).size.width * 1,
              height: 45,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => Configuracao()));
                },
                child: Text(
                  'Configuração',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              width: MediaQuery.of(context).size.width * 1,
              height: 45,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => ListaPesagens()));
                },
                child: Text(
                  'Lista de Pesagens',
                  style: TextStyle(fontSize: 22),
                ),
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                  width: MediaQuery.of(context).size.width * 1,
                  height: 45,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Sair',
                      style: TextStyle(fontSize: 22),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
