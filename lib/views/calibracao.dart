import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Calibracao extends StatefulWidget {
  const Calibracao({Key? key}) : super(key: key);

  @override
  State<Calibracao> createState() => _CalibracaoState();
}

//PROFESSOR O SENHOR NAO CONFIGUROU E ELE ESTA RETORNANDO FALHA, MAS A REQUISICAO ESTA CERTA
dynamic zero = "";
dynamic obterPeso = "";
TextEditingController obterPesoController = TextEditingController();

class _CalibracaoState extends State<Calibracao> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width * 1,
          height: MediaQuery.of(context).size.height * 1,
          child: Column(
            children: [
              Container(
                child: Image.asset(
                  'assets/image/84dd58f2-9e28-4db1-9582-e43d4e685074-profile-picture.jpeg',
                  height: 200,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                width: MediaQuery.of(context).size.width * 1,
                margin: EdgeInsets.only(top: 20),
                child: Row(
                  children: [
                    Text(
                      'Calibração: ',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    Container(
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.red,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                margin: EdgeInsets.only(top: 20),
                width: MediaQuery.of(context).size.width * 1,
                height: 45,
                child: RaisedButton(
                  onPressed: obterZero,
                  child: Text(
                    'Obter Zero',
                    style: TextStyle(fontSize: 22),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: TextField(
                  controller: obterPesoController,
                  decoration: InputDecoration(
                    labelText: 'Informe o peso de calibração:',
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                margin: EdgeInsets.only(top: 20),
                width: MediaQuery.of(context).size.width * 1,
                height: 45,
                child: RaisedButton(
                  onPressed: () async {
                    int teste = int.parse(obterPesoController.text);
                    await pesorReconhecido(teste);

                    print(teste.runtimeType);
                  },
                  child: Text(
                    'Obter Peso de Calibração',
                    style: TextStyle(fontSize: 22),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  "Zero: ${zero}",
                  style: TextStyle(fontSize: 22),
                ),
              ),
              Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  "Calibração: ${obterPeso}",
                  style: TextStyle(fontSize: 22),
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    margin: EdgeInsets.only(bottom: 10),
                    width: MediaQuery.of(context).size.width * 1,
                    height: 45,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Voltar',
                        style: TextStyle(fontSize: 22),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<http.Response> pesorReconhecido(dynamic peso) async {
    String body = """
    {
      "peso": $peso
    }
    """;
    var response = await http.post(
      Uri.parse('http://192.168.4.1/pb/calibracao/ref'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      },
      body: body,
    );
    if (response.statusCode >= 200 && response.statusCode <= 300) {
      obterPeso = json.decode(response.body);
      setState(() {
        obterPeso = obterPeso["calibracao"];
      });
      print(obterPeso);
    }
    return response;
  }

  Future<http.Response> obterZero() async {
    var response = await http.get(
      Uri.parse('http://192.168.4.1/pb/calibracao/zero'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      },
    );
    if (response.statusCode >= 200 && response.statusCode <= 300) {
      zero = json.decode(response.body);
      setState(() {
        zero = zero["zero"];
      });
      print(zero);
    }
    return response;
  }
}
