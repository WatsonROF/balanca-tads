import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Referencias extends StatefulWidget {
  @override
  State<Referencias> createState() => _ReferenciasState();
}

class _ReferenciasState extends State<Referencias> {
  dynamic respostaReferencia;
  TextEditingController minController = TextEditingController();
  TextEditingController maxController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width * 1,
          height: MediaQuery.of(context).size.height * 1,
          child: Column(
            children: [
              Container(
                child: Image.asset(
                  'assets/image/84dd58f2-9e28-4db1-9582-e43d4e685074-profile-picture.jpeg',
                  height: 200,
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                width: MediaQuery.of(context).size.width * 1,
                margin: EdgeInsets.only(top: 20),
                child: Text(
                  'Referência mínima: 60',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                width: MediaQuery.of(context).size.width * 1,
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  'Referência máxima: 700',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                margin: EdgeInsets.only(top: 10),
                child: TextField(
                  controller: minController,
                  decoration: InputDecoration(
                    labelText: 'Informe a referência mínima:',
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                margin: EdgeInsets.only(top: 10),
                child: TextField(
                  controller: maxController,
                  decoration: InputDecoration(
                    labelText: 'Informe a referência máxima:',
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                margin: EdgeInsets.only(top: 20),
                width: MediaQuery.of(context).size.width * 1,
                height: 45,
                child: RaisedButton(
                  onPressed: () async {
                    int valorMin = int.parse(minController.text);
                    int valorMax = int.parse(maxController.text);
                    await referencias(valorMin, valorMax);
                  },
                  child: Text(
                    'Enviar Referências',
                    style: TextStyle(fontSize: 22),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(15),
                child: Text(
                  'Referências: ${respostaReferencia}',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    padding: EdgeInsets.only(left: 15, right: 15),
                    margin: EdgeInsets.only(bottom: 10),
                    width: MediaQuery.of(context).size.width * 1,
                    height: 45,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Voltar',
                        style: TextStyle(fontSize: 22),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<http.Response> referencias(dynamic min, dynamic max) async {
    String body = """
    {
      "min": "$min",
      "max": "$max"
    }
    """;
    var response = await http.post(
      Uri.parse('http://192.168.4.1/pb/referencias'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      },
      body: body,
    );
    if (response.statusCode >= 200 && response.statusCode <= 300) {
      respostaReferencia = json.decode(response.body);
      setState(() {
        respostaReferencia = respostaReferencia["referencias"];
      });
      print(respostaReferencia);
    }
    return response;
  }
}
