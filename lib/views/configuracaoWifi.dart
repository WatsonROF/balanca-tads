import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ConfiguracaoWifi extends StatefulWidget {
  const ConfiguracaoWifi({Key? key}) : super(key: key);

  @override
  State<ConfiguracaoWifi> createState() => _ConfiguracaoWifiState();
}

class _ConfiguracaoWifiState extends State<ConfiguracaoWifi> {
  dynamic dadosUsuario = "VAZIO";
  TextEditingController ssidController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width * 1,
          height: MediaQuery.of(context).size.height * 1,
          child: Column(
            children: [
              Container(
                child: Image.asset(
                  'assets/image/84dd58f2-9e28-4db1-9582-e43d4e685074-profile-picture.jpeg',
                  height: 200,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20, right: 10, left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              'SSID: PersonalBov',
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'Internet: ',
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: Colors.red,
                                  ),
                                  width: 15,
                                  height: 15,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: Text(
                        'SENHA: PersonalBovloT',
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                child: Column(
                  children: [
                    TextField(
                      controller: ssidController,
                      decoration: InputDecoration(
                          labelText: 'Nome Wifi',
                          labelStyle: TextStyle(fontSize: 20)),
                    ),
                    TextField(
                      controller: passwordController,
                      decoration: InputDecoration(
                          labelText: 'Senha Wifi',
                          labelStyle: TextStyle(fontSize: 20)),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      height: 45,
                      width: MediaQuery.of(context).size.width * 1,
                      child: RaisedButton(
                        onPressed: () async {
                          await loginSSID(ssidController, passwordController);
                        },
                        child: Text(
                          'CADASTRAR',
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        dadosUsuario != ""
                            ? 'SSID: ${dadosUsuario}'
                            : 'SSID: ${dadosUsuario}',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 10),
                    height: 45,
                    width: MediaQuery.of(context).size.width * 1,
                    child: RaisedButton(
                      onPressed: () async {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Voltar',
                        style: TextStyle(fontSize: 22),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<http.Response> loginSSID(dynamic ssid, dynamic password) async {
    String body = """
    {
      "ssid": "$ssid",
      "password": "$password"
    }
    """;
    var response = await http.post(
      Uri.parse('http://192.168.4.1/pb/wifi/ssid'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      },
      body: body,
    );
    if (response.statusCode >= 200 && response.statusCode <= 300) {
      dadosUsuario = json.decode(response.body);
      setState(() {
        dadosUsuario = dadosUsuario["ssid"];
      });
      print(dadosUsuario);
    }
    return response;
  }
}
